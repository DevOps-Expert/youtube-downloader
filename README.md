# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
   This will help you to Download any youtube videos, Movies, playlist, etc. very easily from your linux pc [ubuntu,centos,red hat, etc.]
* Version

> Release v0.5

* [Harry](https://www.facebook.com/HarryTheITExpert)

### How do I get set up & Configure? ###

* You have to run below commands on your terminal to install and configuration:

  >   `git clone https://DevOps-Expert@bitbucket.org/DevOps-Expert/youtube-downloader.git`

  >  `cd youtube-downloader`

  >  ` ./ytd --configure; source ~/.bashrc`


### How to Download Youtube Videos ? ###
 > It is very simple and very easy tool that allow you to download youtube videos, playlist, easily. it is also helpful to download bulk videos from file. that contains bulk youtube urls.

 * To Get Syntax Help or Man Page, Execute below one.

> `ytd -h`

> `ytd --help`

* if you want to Download HD Videos (1280x720p) run below command.

>`ytd -u https://www.youtube.com/watch?v=s7viEqT02Yc --hd`

 * Run Below command to Download MP3 from youtube.

> `ytd -u https://www.youtube.com/watch?v=s7viEqT02Yc --mp3`

 * To Download Standard Quality Video from youtube.

> `ytd -u https://www.youtube.com/watch?v=s7viEqT02Yc --video`

 * To Download HD Videos,Playlist,Bulk Videos using `./tmp/video_list.txt` from youtube.

>    `ytd -o HD_Videos --bulk`

>    `ytd -o HD_Videos --playlist`

>    `ytd -o HD_Videos --batch`

  > Note: `-o` used to create a folder name `HD_Videos` & all videos will be Download here. you must have to make entries all urls in `./tmp/video_list.txt` that we want to download.



### Who do I talk to? ###

* Repo owner or admin
  [Harry](HarryTheITEXpert@gmail.com)
* Other community or team contact
