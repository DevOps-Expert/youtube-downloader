#!/usr/bin/env bash

assign_cli_arguments() {

  while [[ $# -gt 0 ]]; do
    case "${1}" in
      -h|--help)
      usage;;
      --configure|--config)
        dependency_check
        check_output_folder
        configure_downloader

        configure_success
        exit 1
      ;;
      -u|--URL)
        YTD_URL=${2}
        export YTD_URL
      shift 2;;
      -o|--output)
        YTD_FOLDER=${2}
        export YTD_FOLDER
      shift 2;;
      --video|--mp3|--hd|--playlist|--bulk|--batch|--test)
        YTD_MODE="${1//--/}"
      shift;;
      *)
        echo -e "${RED}FATAL: Unknown command-line argument or environment: ${1}${NC}"
        exit 1
    esac
  done
}
