#!/usr/bin/env bash

#--video|--mp3|--hd|--playlist|--test

usage() {
  echo "Usage: $(basename ${0}) [--(hd|video|playlist|batch|mp3|bulk)]"
  echo "         -u {URL} "
  echo ""

  echo " --URL          -u  -  Video URL must be provided"
  echo " --output       -o   - Output Folder Name eg. NEW_Videos"
  echo ""

  echo " --hd                - Download HD Video from youtube URL"
  echo " --mp3               - Download Mp3 file Direct from youtube"
  echo " --playlist          - Download Bulk Videos From file."
  echo " --bulk              - Download Bulk Videos From file."
  echo " --batch             - Download Bulk Videos From file."
  echo " --video             - Just Download Video"
  # echo " --ansible           - Only run ansible (--playbook option mandatory)"
  # echo " --deploy            - Only run deploy node app"
  # echo " --package           - Package application put on Nexus"
  # echo ""
  # echo "If more than one of plan, apply, run, destory, show, or ansible, is passed"
  # echo "the last one on the command line will take precidence over all others."
  # echo ""
  # echo "Options for --run:"
  # echo " --jenkins       -j  - Disable interactive confirmations and SSH key checks"
  # echo " --yes           -y  - Don't ask for confirmation after running plan (with --run)"
  # echo " --update        -p  - Update any external modules in this run (new ones will"
  # echo "                       still be fetched prior to running regardless)"
  # echo " --wait          -w  - Time to wait between connection attempts to instances"
  # echo " --playbook          - Ansible playbook to run"
  # echo " --tags          -t  - Tags to pass to Ansible"
  # echo " --check         -c  - Dry-run the changes in Ansible"
  # echo " --diff          -d  - Output diff changes in Ansible"
  # echo " --verbose       -v  - Verbose output from Ansible"
  echo ""
  echo " --help          -h  - This message."
  echo ""

  echo ""
  echo "How to Download from Youtube?"
  echo "    Download HD Video:     - ytd -u <YOUTUBE_URL> --hd"
  echo "    Download Video:        - ytd -u <YOUTUBE_URL> --video"
  echo "    Download MP3:          - ytd -u <YOUTUBE_URL> --mp3"
  echo ""
  echo "How to Download Multiple Videos or Complete playlist?"
  echo "    Download bulk videos,playlist,urls:        - ytd -o <foldername> --playlist|--bulk|--batch"
  echo "    eg. ytd -o Movies --bulk"
  echo "    Note:- Paste Videos url in $BULK_DOWNLOAD"
  echo ""





  exit -1
}

#Download Bulk Videos From file that contain lots of video urls.
#./main -o HDvideo --playlist
