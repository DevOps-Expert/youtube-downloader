#!/usr/bin/env bash

dependency_check(){
  #  echo "Checking Dependency ${FUNCNAME}"
  if [[ -f ${YOUTUBE_BIN} ]]; then
    #check if youtube-dl file is available.
    echo -e "${GREEN}Dependency Checked Successfuly${NC}"

  else
    echo "Binary file not found for video Downloading"
    echo ""
    echo -e "${GREEN}Installing Binary file ${NC}"
    sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o ${YOUTUBE_BIN}
    # wget https://yt-dl.org/downloads/latest/youtube-dl -O ${YOUTUBE_BIN}
    sudo chmod a+rx ${YOUTUBE_BIN}
  fi
}

check_output_folder(){
  #  echo "Checking Folder:  ${FUNCNAME}"
  if [ ! -d "$OUTPUT_PATH" ]; then
    mkdir -p ${OUTPUT_PATH}/{mp3,video,hd}
  fi
}

configure_downloader(){
  #  echo "Configuring:  ${FUNCNAME}
  #echo "alias ytd=${PROJECT_ROOT}/ytd"
  sed --in-place '/alias ytd/d' ~/.bash_aliases
  #echo "alias ytd='${PROJECT_ROOT}/ytd'" >> ~/.bash_aliases
  #echo "export YTD_HOME=${PROJECT_ROOT}" >> ~/.bashrc
  echo "alias ytd='pushd ${PROJECT_ROOT} && ./ytd'" >> ~/.bash_aliases
  #echo bash -c "cd ${PROJECT_ROOT}; git pull origin master" >> ~/.bashrc
  #source ~/.bash_aliases
  #echo "alias ytd='${PROJECT_ROOT}/ytd'" >> ~/.bashrc
}

configure_success(){
  banner "Thanks"
  echo "               Created by Harry"
  echo "Your Downloader Configured"
  echo ""
  echo -e "${GREEN}ytd -h              -     Execute for help${NC}"
  echo ""
  echo ""
  #usage

}
