#!/bin/bash
#info > /dev/null 2>&1

youtube_mode() {
  echo "Switching Mode: ${FUNCNAME}"
  export VID=`echo $YTD_URL |cut -d "=" -f2`
  info >  /dev/null 2>&1
  case $YTD_MODE in
    video)
      echo "Video is Downloading"
      echo -e "${BLUE}Download Location - ${OUTPUT_PATH}/${YTD_MODE} ${NC}"
      ${YOUTUBE_BIN} -f ${VIDEO_QUALITY} $VID --restrict-filenames -o ${OUTPUT_PATH}/${YTD_MODE}/'%(title)s'-${CREDITS}.'%(ext)s'
    ;;
    mp3)
      echo "Please Wait Fetching MP3 From Server"
      echo ""
      echo -e "${BLUE}Download Location - ${OUTPUT_PATH}/${YTD_MODE} ${NC}"
      ${YOUTUBE_BIN} --restrict-filenames --extract-audio --audio-format mp3 $VID -o ${OUTPUT_PATH}/${YTD_MODE}/'%(title)s'-${CREDITS}.'%(ext)s'
    ;;
    playlist|bulk|batch)
      echo "Downloading Playlist and multiple files"
      if [[ ! -d ${OUTPUT_PATH}/${YTD_FOLDER} ]]; then
        mkdir -p ${OUTPUT_PATH}/${YTD_FOLDER}
      fi

      echo -e "${BLUE}Download Location - ${OUTPUT_PATH}/${YTD_FOLDER} ${NC}"
      ${YOUTUBE_BIN} -a $BULK_DOWNLOAD -f ${VIDEO_QUALITY} --restrict-filenames -o ${OUTPUT_PATH}/${YTD_FOLDER}/'%(title)s'-${CREDITS}.'%(ext)s'
    ;;
    hd)
      echo "HD Video Downloading Now."
      echo -e "${BLUE}Download Location - ${OUTPUT_PATH}/${YTD_MODE} ${NC}"
      #youtube-dl -F $YTD_URL  > ${TEMP_FILE}
      FORMAT=`${YOUTUBE_BIN} -F $YTD_URL | grep "best" | awk '{print $1}'`
      #EXT=.`youtube-dl --get-filename -f $FORMAT $YTD_URL | cut -d "." -f2`
      #echo  "youtube-dl -f $FORMAT $YTD_URL -o ${OUTPUT_PATH}/%(title)s-${CREDITS}.%(ext)s"
      ${YOUTUBE_BIN} -f $FORMAT $VID --restrict-filenames -o ${OUTPUT_PATH}/${YTD_MODE}/'%(title)s'-${CREDITS}.'%(ext)s'
    ;;
    test)
      echo "Test Video Downloading Now."
    ;;
    *)
      echo "Unknown query found"
    ;;
  esac
}
